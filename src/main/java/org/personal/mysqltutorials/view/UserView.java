package org.personal.mysqltutorials.view;

import java.util.List;
import java.util.Scanner;

import org.personal.mysqltutorials.dao.UserInterface;
import org.personal.mysqltutorials.dao.impl.UserDaoImpl;
import org.personal.mysqltutorials.model.User;

public class UserView {

	final Scanner scan = new Scanner(System.in);

	UserInterface userInterface = new UserDaoImpl();

	public UserView() {
		displaySwitch();
	}

	public void displaySwitch() {
		String cont = null;
		do {

			System.out.println("1. Insert");
			System.out.println("2. Update");
			System.out.println("3. Delete");
			System.out.println("4. Display");
			int choice = scan.nextInt();

			switch (choice) {

			case 1:
				System.out.println("Enter fname");
				String fname = scan.next();
				System.out.println("Enter lname");
				String lname = scan.next();
				System.out.println("Enter address");
				String address = scan.next();
				User user = new User(fname, lname, address);

				try {
					int check = userInterface.insert(user);
					if (check >= 1) {
						System.out.println("Insert Sucess");
					} else {
						System.out.println("Insert Failed");
					}
				} catch (Exception e) {
					System.out.println("exception message " + e.getMessage());
				}

				break;
			case 2:
				System.out.println("Enter userId to update");
				int userId = scan.nextInt();
				try {
					User userOne = userInterface.findOne(userId);
					if (userOne != null) {
						System.out.println(userOne.toString());
						System.out.println("Enter fname");
						String fnameUpdate = scan.next();
						System.out.println("Enter lname");
						String lnameUpdate = scan.next();
						System.out.println("Enter address");
						String addressUpdate = scan.next();
						User userUpdate = new User(userId, fnameUpdate, lnameUpdate, addressUpdate);
						int check = userInterface.update(userUpdate);
						if (check >= 1) {
							System.out.println("update sucessfull");
						} else {
							System.out.println("update failed");
						}
					} else {
						System.out.println("User Id not found");
					}
				} catch (Exception e1) {
					System.out.println("exception message " + e1.getMessage());
				}
				break;

			case 3:
				System.out.println("Enter userId to delete");
				int userIdDelete = scan.nextInt();
				try {
					User userOne = userInterface.findOne(userIdDelete);
					if (userOne != null) {

						int check = userInterface.delete(userOne);
						if (check >= 1) {
							System.out.println("delete sucessfull");
						} else {
							System.out.println("delete failed");
						}
					} else {
						System.out.println("User Id not found");
					}
				} catch (Exception e1) {
					System.out.println("exception message " + e1.getMessage());
				}

				break;

			case 4:
				try {
					List<User> listOfUsers = userInterface.findAll();

					for (User listOfUser : listOfUsers) {
						System.out.println("firstname " + listOfUser.getFname());
						System.out.println("lastname " + listOfUser.getLname());
						System.out.println("address " + listOfUser.getAddress());
					}
				} catch (Exception e) {
					System.out.println("exception message " + e.getMessage());
				}

				break;

			default:
				System.out.println("Invalid choice");

			}
			System.out.println("Continue (Y/y)");
			cont = scan.next();

		} while (cont.equalsIgnoreCase("y"));
	}

}
