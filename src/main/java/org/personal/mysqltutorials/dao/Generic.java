package org.personal.mysqltutorials.dao;

import java.util.List;

public interface Generic <T> {
	
	public int insert(T t) throws Exception;
	
	public int update(T t) throws Exception;
	
	public int delete(T t) throws Exception;
	
	public T findOne(int id) throws Exception;
	
	public List<T> findAll() throws Exception;

}
