package org.personal.mysqltutorials.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

import org.personal.mysqltutorials.connection.ConnectionFactory;
import org.personal.mysqltutorials.dao.UserInterface;
import org.personal.mysqltutorials.model.User;

public class UserDaoImpl implements UserInterface {

	private final Connection connection;

	public UserDaoImpl() {
		connection = ConnectionFactory.getconnection();
	}

	public int insert(User user) throws Exception {
		PreparedStatement preparedStatement = connection
				.prepareStatement("insert into userinfo (firstname, lastname, address ) values (?,?,?)");
		preparedStatement.setString(1, user.getFname());
		preparedStatement.setString(2, user.getLname());
		preparedStatement.setString(3, user.getAddress());
		return preparedStatement.executeUpdate();
	}

	@Override
	public int update(User t) throws Exception {
		PreparedStatement preparedStatement = connection
				.prepareStatement("update userinfo set firstname = ?, lastname = ?, address = ? where id = ?");
		preparedStatement.setString(1, t.getFname());
		preparedStatement.setString(2, t.getLname());
		preparedStatement.setString(3, t.getAddress());
		preparedStatement.setInt(4, t.getId());
		return preparedStatement.executeUpdate();
	}

	@Override
	public int delete(User t) throws Exception {
		PreparedStatement preparedStatement = connection.prepareStatement("DELETE from userinfo where id=?");
		preparedStatement.setInt(1, t.getId());
		return preparedStatement.executeUpdate();
	}

	@Override
	public User findOne(int id) throws Exception {

		User user = new User();
		PreparedStatement preparedStatement = connection
				.prepareStatement("select id, firstname, lastname, address from userinfo where id = ?");
		preparedStatement.setInt(1, id);
		ResultSet resultSet = preparedStatement.executeQuery();
		while (resultSet.next()) {
			getResultSet(user, resultSet);
		}
		return user;
	}

	private void getResultSet(User user, ResultSet resultSet) throws SQLException {
		user.setId(resultSet.getInt("id"));
		user.setFname(resultSet.getString("firstname"));
		user.setLname(resultSet.getString("lastname"));
		user.setAddress(resultSet.getString("address"));
	}

	@Override
	public List<User> findAll() throws Exception {
		List<User> userList = new LinkedList<>();
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery("select *from userinfo");
		while (resultSet.next()) {
			User user = new User();
			getResultSet(user, resultSet);
			userList.add(user);
		}
		return userList;
	}
}
