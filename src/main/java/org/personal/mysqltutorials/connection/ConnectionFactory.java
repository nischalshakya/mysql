package org.personal.mysqltutorials.connection;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionFactory {

	public static Connection getconnection() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			return DriverManager.getConnection("jdbc:mysql://localhost:3306/mysqltutorials", "root", "root");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
